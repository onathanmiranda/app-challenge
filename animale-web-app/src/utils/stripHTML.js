export default function stripHTML( dirtyString ){
    const doc = new DOMParser().parseFromString(dirtyString, 'text/html')
    return doc.body.textContent || ''
}