export default function updateQueryParam(key, value){
    
    const URI = new URL( window.location.href )
    URI.searchParams.set(key, value)
    return URI.href.replace(URI.origin, "")
}