import { Switch, Route, BrowserRouter } from 'react-router-dom'

import Navbar from './components/organism/navbar'

import Home     from './components/screens/home'
import Menu     from './components/screens/menu'
import MyList   from './components/screens/my-list'
import Conta    from './components/screens/conta'
import Sacola   from './components/screens/sacola'
import Category from './components/screens/category'
import Product  from './components/screens/product'

export default function Routes(){
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/menu" exact component={Menu} />
                <Route path="/categorias/:category" component={Category} />
                <Route path="/product/:linkText" component={Product} />
                <Route path="/my-list" exact component={MyList} />
                <Route path="/conta" exact component={Conta} />
                <Route path="/sacola" exact component={Sacola} />
            </Switch>
            <Navbar />
        </BrowserRouter>
    )
}