//Simulação de Consultas a uma API
import products from './products.json'
import slugify  from 'slugify'

const slugfy = (str) => slugify( str, { lower: true, strict: true })

function splitUp(arr, n) {
    //http://jsfiddle.net/ht22q/
    var rest = arr.length % n,
        restUsed = rest,
        partLength = Math.floor(arr.length / n),
        result = [];
    
    for(var i = 0; i < arr.length; i += partLength) {
        var end = partLength + i,
            add = false;
        
        if(rest !== 0 && restUsed) {
            end++;
            restUsed--;
            add = true;
        }
        
        result.push(arr.slice(i, end));
        
        if(add) {
            i++;
        }
    }
    
    return result;
}

class Products {
    constructor(){
        this.get = ({ page, per_page }) => new Promise(( resolve ) => {

            const totalPages            = Math.ceil(products.length / per_page)
            const productsDividedByPage = splitUp(products, totalPages)
            const items                 = productsDividedByPage[page - 1]
            const nextPage              = page + 1

            resolve({ 
                data: { 
                    items, 
                    totalPages, 
                    page: nextPage 
                }
            })
        })

        this.getByCategory = ({ page, per_page, category }) => new Promise(( resolve, reject ) => {

            const _products = products.filter(( product ) => {
                
                const productCategories = product.categories.map(( cat ) => slugfy( cat ))
                const _category = slugfy( category )
                return productCategories.includes( _category )
            })

            if(_products.length === 0){
                reject({
                    data: {
                        error: new Error("Not Found"),
                        status: 404
                    }
                })
            } else {
                const totalPages            = Math.ceil(_products.length / per_page)
                const productsDividedByPage = splitUp(_products, totalPages)
                const items                 = productsDividedByPage[page - 1]
                const nextPage              = page + 1

                resolve({
                    data: {
                        items, 
                        totalPages, 
                        page: nextPage 
                    }
                })
            }
        }) 

        this.getByLinkText = ({ page, per_page, linkText }) => new Promise(( resolve, reject ) => {

            const _products = products.filter(( product ) => product.linkText === linkText )

            if(_products.length === 0){
                reject({
                    data: {
                        error: new Error("Not Found"),
                        status: 404
                    }
                })
            } else {
                const totalPages            = Math.ceil(_products.length / per_page)
                const productsDividedByPage = splitUp(_products, totalPages)
                const items                 = productsDividedByPage[page - 1]
                const nextPage              = page + 1

                resolve({
                    data: {
                        items, 
                        totalPages, 
                        page: nextPage 
                    }
                })
            }
        }) 

        this.search = ({ query, per_page, page }) => new Promise(( resolve, reject ) => {
            
            const result = products.filter( product => {

                //Will case insensitively compare search query with title and categories of products

                //LowerCases query
                const term = query.toLowerCase()
                
                //Get Product Name and LowerCases it
                const productName = product.productName.toLowerCase()
                
                //Get Product Categories and LowerCases them
                const categories = product.categories.reduce((acc, cat) => {
                    
                    const _categories = cat.split("/").filter(( category ) => ( category !== "" && !acc.includes( category )))           
                    
                    return [ ...acc, ..._categories ]

                }, []).map((category) => category.toLowerCase())
                
                //return matching 
                return productName.includes(term) || categories.includes(term)
            })
            
            if(result.length === 0){
                reject({
                    data: {
                        error: new Error("Not Found"),
                        status: 404
                    }
                })
            }

            const totalPages            = (result.length) ? Math.ceil(result.length / per_page) : 1
            const productsDividedByPage = (result.length) ? splitUp(result, totalPages) : []
            const items                 = (result.length) ? productsDividedByPage[page - 1] : []
            const nextPage              = page === totalPages ? page : page + 1
            
            resolve({
                data: { 
                    items, 
                    totalPages, 
                    page: nextPage 
                }
            })
        })
    }
}

export default new Products()