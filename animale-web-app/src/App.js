import { Provider }  from 'react-redux'

import Routes from './_routes'
import store  from './store'

export default function App() {
    return (
        <Provider store={store}>
            <Routes />
        </Provider>
    );
}
