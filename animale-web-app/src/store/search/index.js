import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import Products from '../../api/products'

const namespace = "search"

const URLSearchParam = "s"
const URI            = new URL( window.location.href )
const searchValue    = URI.searchParams.get( URLSearchParam ) || ""

const initialState = {
    query: searchValue,
    URLSearchParam,
    requesting: false,
    page:       1,      //API page
    totalPages: 1,      //API total pages
    per_page:   60,     //API products per page
    result:     false,
}

const searchProducts = createAsyncThunk(
    `${ namespace }/searchProducts`,
    ( args, thunkAPI ) => {

        const { query, page, per_page } = thunkAPI.getState().search
        
        return (
            Products.search({ query, page, per_page })
            .then( response => response.data )
            .catch((e) => {
                return Promise.reject("")
            })
        )
    }
)

const slice = createSlice({
    name: namespace,
    initialState,
    reducers: {
        setQuery: (state, action) => {
            state.query = action.payload
        },
        clearSearch: (state, action) => {
            state.query = ""
            state.result = false
        }
    },
    extraReducers: {
        [ searchProducts.fulfilled ]: ( state, action ) => {
            
            state.requesting    = false
            state.result        = action.payload.items
            state.page          = action.payload.page
            state.totalPages    = action.payload.totalPages
        },
        [ searchProducts.pending ]: ( state, action ) => {
            state.requesting = true
            state.result     = false
        },
        [ searchProducts.rejected ]: ( state, action ) => {
            state.requesting = false
            state.result     = []
        }
    }
})

const { actions, reducer } = slice

export const { setQuery, clearSearch } = actions

export { reducer, searchProducts }

export default slice