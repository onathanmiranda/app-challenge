import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import Products from '../../api/products'

const namespace = "products"

const initialState = {
    items:      [],     //array of products
    page:       1,      //API page
    totalPages: 1,      //API total pages
    per_page:   6,      //API products per page
    requesting: true    //is currently requesting (starts on true for first load)
}

const getByCategory = createAsyncThunk(
    `${ namespace }/getByCategory`,
    ( args, thunkAPI ) => {
        
        const state                             = thunkAPI.getState()
        const { page, totalPages, per_page }    = state[ namespace ]
        const { category }                      = args
        
        if( page > totalPages ) 
            return Promise.reject( "No more items" )

        return Products.getByCategory({ page, per_page, category }).then(( response ) => response.data )
    }
)

const getByLinkText = createAsyncThunk(
    `${ namespace }/getByLinkText`,
    ( args, thunkAPI ) => {
        
        const state                             = thunkAPI.getState()
        const { page, totalPages, per_page }    = state[ namespace ]
        const { linkText }                      = args
        
        if( page > totalPages ) 
            return Promise.reject( "No more items" )

        return Products.getByLinkText({ page, per_page, linkText }).then(( response ) => response.data )
    }
)

//To Do: Avoid consulting API to load items that have already been loaded from "API"
const slice = createSlice({
    name: namespace,
    initialState,
    reducers: {
        resetState: () => initialState
    },
    extraReducers: {
        //GET BY CATEGORY HANDLERS
        [getByCategory.fulfilled]: ( state, action ) => {
            
            state.requesting    = false
            state.items         = [ ...state.items, ...action.payload.items ]
            state.page          = action.payload.page
            state.totalPages    = action.payload.totalPages
        },
        [getByCategory.pending]: ( state, action ) => {
            //Should not have to reset state
            if(state.page === 1){
                state = initialState
            }
        },
        [getByCategory.rejected]: ( state, action ) => {
            state.requesting = false
        },

        //GET BY LINK TEXT
        [getByLinkText.fulfilled]: ( state, action ) => {
            
            state.requesting    = false
            state.items         = [ ...state.items, ...action.payload.items ]
            state.page          = action.payload.page
            state.totalPages    = action.payload.totalPages
        },
        [getByLinkText.pending]: ( state, action ) => {
            //Should not have to reset state
            if(state.page === 1){
                state = initialState
            }
        },
        [getByLinkText.rejected]: ( state, action ) => {
            state.requesting = false
        }
    }
})

const { resetState } = slice.actions

export { 
    getByCategory,
    getByLinkText,
    resetState
}

export const { reducer } = slice

export default slice