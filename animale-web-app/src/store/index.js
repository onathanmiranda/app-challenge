import { configureStore } from '@reduxjs/toolkit'

import { reducer as products }  from './products'
import { reducer as search }    from './search'

export default configureStore({
    reducer: {
        products,
        search
    },
    devTools: !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
})