import { Link } from "react-router-dom";

import style from './style.module.scss'

export default function RoundCategoryLink({ title, to, img }){
    return(
        <Link to={to} className={ style.link }>

            <img className={ style.img } src={ img } alt={`${ title } | Animale`}  title={`${ title } | Animale`} />
            <div className={ style.title }>
                { title }
            </div>
        </Link>
    )
}