import { Link } from "react-router-dom"

import { IconArrowRight } from '../../atom/icon'

import style from './style.module.scss'

export default function HorizontalLink( props ){
    
    const Icon = props.icon

    return(
        <Link className={ style.link } to={ props.to }>
            
            <Icon className={ style.icon }/>

            <div className={ style.text }>
                { props.children }
            </div>
            
            <IconArrowRight className={ style.arrow } />
        </Link>
    )
}