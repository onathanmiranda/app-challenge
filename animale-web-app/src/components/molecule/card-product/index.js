import { Link } from 'react-router-dom'

import formatCurrency from '../../../utils/formatCurrency'

import style from './style.module.scss'


export default function ProductCard({ product, className }){
   
    const { productName }       = product
    const productImageSource    = product.items[0].images[0].imageUrl
    const status                = product.SALE[0] === "SIM" ? "SALE" : "" //not sure

    const { commertialOffer }   = product.items[0].sellers[0]
    const price                 = commertialOffer.Price
    const priceWithoutDiscount  = commertialOffer.PriceWithoutDiscount
    const showfullPrice         = price !== priceWithoutDiscount
    const isAvailable           = Boolean( commertialOffer.AvailableQuantity )
    
    const maxInstallmentsNumber         = commertialOffer.Installments.reduce(( acc, installment ) => Math.max( acc, installment.NumberOfInstallments ), 1)
    const [ installment ]               = commertialOffer.Installments.filter((installment) => installment.NumberOfInstallments === maxInstallmentsNumber )
    const NumberOfInstallments          = installment ? installment.NumberOfInstallments : 0
    const TotalValuePlusInterestRate    = installment ? installment.TotalValuePlusInterestRate : 0
    const installmentValue              = installment ? (TotalValuePlusInterestRate / NumberOfInstallments).toFixed(2) : 0

    return (
        <article className={`${ style.card } ${ className || "" }`}>
            <Link to={`/product/${product.linkText}`}>
                <figure className={ style.figure }>
                    <img className={ style.img } src={ productImageSource } alt=""/>
                </figure>

                <section className={ style.productInfo }>

                    <div className={ style.status }>{ status }</div>
                    <h2 className={ style.productName }>{ productName }</h2>

                    {isAvailable && 
                        <>
                            <p className={ style.price }>
                                {formatCurrency(price)} { showfullPrice && <span>{formatCurrency(priceWithoutDiscount)}</span>}
                            </p>
                            {Boolean(installment) && 
                                <p className={ style.installments }>
                                    { NumberOfInstallments }x de {formatCurrency(installmentValue)}
                                </p>
                            }
                        </>
                    }

                    {!isAvailable &&
                        <p className={ style.unavailable }>
                            Esgotado
                        </p>
                    }
                </section>
            </Link>
        </article>
    )
}