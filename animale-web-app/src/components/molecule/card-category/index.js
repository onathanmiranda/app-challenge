import Title    from '../../atom/title';
import LinkCTA  from "../../atom/link-cta";

import style from './style.module.scss'


export default function CardCategory( props ){
    return (
        <article className={`${ style.card } ${ props.className || "" }`}>

            <div className={ style.card_aspectRatio }>

                <div style={{ backgroundImage: `url(${props.featuredImage})`}} className={ style.card_content }>
                    <div className={ style.content_wrapper }>
                        {Boolean( props.subtitle ) &&
                            <p className={ style.card_subtitle }>{ props.subtitle }</p>
                        }
                        {Boolean( props.title ) && 
                            <Title className={ style.card_title }>{ props.title }</Title>
                        }
                        {Boolean( props.to ) &&
                            <LinkCTA className={ style.callToAction } to={ props.to }>Shop Now</LinkCTA>
                        }
                    </div>
                </div>
            </div>
        </article>
    )
}