import { Link } from "react-router-dom";

import style from './style.module.scss'

export default function HorizontalCategoryLink({ title, to, img, className = "" }){
    return(
        <Link to={to} className={`${ style.link } ${ className }`}>
            <div className={ style.title }>
                { title }
            </div>
            <img className={ style.img } src={ img } alt={`${ title } | Animale`}  title={`${ title } | Animale`} />
        </Link>
    )
}