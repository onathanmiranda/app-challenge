import { Link } from 'react-router-dom'

import style from './style.module.scss'

export default function LinkCTA({ className, to, children }){
    return(
        <Link className={`${ style.button } ${ className || "" }`} to={ to }>{ children }</Link>
    )
}