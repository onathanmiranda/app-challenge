import * as React from "react";

function SvgIconBag(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={16.8}
      height={18.14}
      {...props}
    >
      <defs>
        <style>
          {".IconBag_svg__a{fill:none;stroke-miterlimit:10}"}
        </style>
      </defs>
      <g className="IconBag_svg__a">
        <path d="M0 5.54h16.8v9.6a3 3 0 01-3 3H3a3 3 0 01-3-3v-9.6z" />
        <path
          d="M1 6.04h14.8a.5.5 0 01.5.5v8.6a2.5 2.5 0 01-2.5 2.5H3a2.5 2.5 0 01-2.5-2.5v-8.6a.5.5 0 01.5-.5z"
          fill="none"
        />
      </g>
      <path
        className="IconBag_svg__a"
        d="M5.04 8.9V4.28A3.589 3.589 0 018.4.5a3.589 3.589 0 013.36 3.78v3.78"
      />
    </svg>
  );
}

export default SvgIconBag;