import * as React from "react";

function SvgIconLocation(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={26.619}
      height={28.354}
      {...props}
    >
      <g strokeMiterlimit={10} fill="none">
        <path stroke="#000" opacity=".6" d="M18.568 6.986c-.05-.05-.102-.1-.154-.147-1.231-1.122-3.257-1.793-5.418-1.793s-4.187.67-5.418 1.793c-1.186 1.082-1.975 2.993-2.11 5.112-.135 2.119.405 4.114 1.444 5.337l6.084 7.16 6.084-7.16c1.013-1.192 1.556-3.138 1.452-5.204-.104-2.067-.839-3.972-1.964-5.098m.707-.707c2.753 2.753 3.074 8.707.567 11.657l-6.846 8.056-6.846-8.056C3.588 14.92 3.98 8.766 6.904 6.1c3.004-2.738 9.18-2.738 12.184 0a5.6 5.6 0 01.187.179z" />
        <g transform="translate(8.619 8.354)" stroke="#000">
          <circle stroke="#000" opacity=".6" cx={4.308} cy={4.308} r={3.808} />
        </g>
      </g>
    </svg>
  );
}

export default SvgIconLocation;
