import * as React from "react";

function SvgIconChat(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={26} height={26} {...props}>
      <defs>
        <style>
          {".icon_chat_svg__b{fill:none;stroke:#000;stroke-miterlimit:10}"}
        </style>
      </defs>
      <g>
        <path className="icon_chat_svg__b" d="M4 14H1V1h16v13H8.5L4 18z" />
        <path
          className="icon_chat_svg__b"
          d="M9 17.275V19h8l5 4v-4h3V6h-5.326"
        />
      </g>
    </svg>
  );
}

export default SvgIconChat;
