import * as React from "react";

function SvgArrow(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={7.92}
      height={7.92}
      {...props}
    >
      <path
        data-name="Path Copy 2"
        d="M3.536 7.495L7.072 3.96 3.536.424h0"
        fill="none"
        stroke="#000"
        strokeMiterlimit={10}
        strokeWidth={1.2}
      />
    </svg>
  );
}

export default SvgArrow;