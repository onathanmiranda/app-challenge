import * as React from "react";

function SvgIconSearch(props) {
    return (
        <svg className={ props.className || "" } xmlns="http://www.w3.org/2000/svg" width="17" height="19" viewBox="0 0 17 19">
            <g id="icon_search" opacity="0.5">
                <g fill="none" stroke="#000" strokeMiterlimit="10" strokeWidth="1">
                <circle cx="8" cy="8" r="8" stroke="none"/>
                <circle cx="8" cy="8" r="7.5" fill="none"/>
                </g>
                <path d="M0,0,3.536,3.536" transform="translate(12.5 14.5)" fill="none" stroke="#000" strokeLinecap="square" strokeMiterlimit="10" strokeWidth="1"/>
            </g>
        </svg>
    );
}

export default SvgIconSearch;