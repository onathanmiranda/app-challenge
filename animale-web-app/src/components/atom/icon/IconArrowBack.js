import * as React from 'react'

function SvgArrowBack({ className = "" }) {
    return (
        <svg 
            xmlns="http://www.w3.org/2000/svg" 
            className={className} 
            width="13.925" 
            height="13.789" 
            viewBox="0 0 13.925 13.789"
        >
            <g transform="translate(0.925 0.394)">
                <path fill="none" stroke="#000" strokeMiterlimit="10" strokeWidth="1.5px" d="M9,9H0V0H0" transform="translate(6.5 0.136) rotate(45)" />
            </g>
        </svg>
    )
}

export default SvgArrowBack