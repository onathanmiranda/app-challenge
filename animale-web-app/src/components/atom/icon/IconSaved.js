import * as React from "react";

function SvgIconSaved(props) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width={25} height={25} {...props}>
            <g>
                <path fill="transparent" d="M0 0h25v25H0z" />
                <g fill="none" stroke="#000" strokeLinejoin="bevel" strokeMiterlimit={10}>
                    <path d="M4 6h17v19l-8.5-6.3L4 25z" />
                </g>
            </g>
        </svg>
    );
}

export default SvgIconSaved;
