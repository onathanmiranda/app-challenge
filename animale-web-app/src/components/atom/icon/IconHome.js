import * as React from "react";

function SvgIconHome(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={25} height={25} {...props}>
      <path fill="transparent" d="M0 0h25v25H0z" />
      <g fill="none" stroke="#000" strokeLinejoin="bevel" strokeMiterlimit={10}>
        <path d="M3 24.7h18v-13L12 4l-9 7.7z" />
      </g>
    </svg>
  );
}

export default SvgIconHome;
