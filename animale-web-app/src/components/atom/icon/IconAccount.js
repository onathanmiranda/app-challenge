import * as React from "react";

function SvgIconAccount(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={25.187}
      {...props}
    >
      <defs>
        <style>
          {
            ".icon_account_svg__c{fill:none;stroke:#000;stroke-linejoin:round;stroke-miterlimit:10}"
          }
        </style>
      </defs>
      <g>
        <path fill="transparent" d="M0 0h25v25H0z" />
        <g className="icon_account_svg__c" transform="translate(6.624 6)">
          <circle cx={5.5} cy={5.5} r={5.5} />
        </g>
        <path
          className="icon_account_svg__c"
          d="M20.263 25.042A8.5 8.5 0 004 24.993"
        />
      </g>
    </svg>
  );
}

export default SvgIconAccount;
