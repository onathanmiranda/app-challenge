import * as React from "react";

function SvgIconMenu(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={25} height={25} {...props}>
      <g>
        <path fill="transparent" d="M0 0h25v25H0z" />
        <path
          d="M5.5 8.5h15"
          fill="none"
          stroke="#000"
          strokeLinecap="square"
          strokeMiterlimit={10}
        />
        <path d="M5 8h16v1H5zM5 14h12v1H5zM5 20h8v1H5z" />
      </g>
    </svg>
  );
}

export default SvgIconMenu;
