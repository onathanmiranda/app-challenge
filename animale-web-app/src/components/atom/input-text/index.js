import style from './style.module.scss'

export default function InputText({ value="", onChange, placeholder="", className="" }){

    const handleOnChange = ( event ) => {
        const { value } = event.target
        onChange( value )
    }

    return (
        <input type="text" value={ value } onChange={ handleOnChange } placeholder={ placeholder } className={`${style.input} ${className}`} />
    )
}