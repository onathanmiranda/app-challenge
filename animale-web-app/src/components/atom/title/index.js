import style from './style.module.scss'

export default function Title(props){
    return <h2 className={`${style.title} ${props.className || ""}`}>{props.children}</h2>
}