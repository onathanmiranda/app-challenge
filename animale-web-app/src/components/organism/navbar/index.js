import { Link, withRouter } from 'react-router-dom'

import { IconHome, IconMenu, IconSaved, IconAccount } from '../../atom/icon'

import style from './style.module.scss'

const menuItems = [
    {
        to: "/",
        Icon: IconHome,
        text: "Início"
    },
    {
        to: "/menu",
        Icon: IconMenu,
        text: "Menu"
    },
    {
        to: "/my-list",
        Icon: IconSaved,
        text: "My List"
    },
    {
        to: "/conta",
        Icon: IconAccount,
        text: "Conta"
    }
]

export default withRouter(function Navbar(props){

    const { pathname } = props.location
    
    return(
        <nav className={style.nav}>
            <ul className={style.menu}>
                
                {menuItems.map((menuItem, key) => {

                    const { Icon, to, text } = menuItem
                    
                    const isMenuCategoryCase = pathname.includes("/categorias/") && to.includes("/menu")
                    
                    const current = pathname === to || isMenuCategoryCase

                    return (
                        <li key={ key } className={`${ style.menuItem } ${ current ? style.current : "" }`}>
                            <Link to={ to } className={ style.menuItem_link }>
                                <Icon className={ style.menuItem_icon } />
                                { text }
                            </Link>
                        </li>
                    )
                })}
            </ul>
        </nav>
    )
})