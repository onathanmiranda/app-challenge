import { Link, withRouter } from 'react-router-dom'

import { IconBag, IconArrowBack } from '../../atom/icon'

import style from './style.module.scss'

export default withRouter(function Header(props){
    return (
        <header className={ `${ style.header } ${ style[props.color] || "" } ${ props.className || "" }` }>
            
            { props.back && 
                <Link onClick={(e) => { 
                    e.preventDefault(); 
                    props.history.goBack()
                }} to="#">
                    <IconArrowBack/>
                </Link> 
            }

            { props.title && 
                <h1 className={ style.title }>{ props.title }</h1> }
            
            <Link to="/sacola" className={ style.link }>
                <IconBag className={ style.bag }/>
            </Link>
        </header>
    )
})