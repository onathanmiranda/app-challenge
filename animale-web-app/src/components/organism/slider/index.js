import { useState }     from 'react'
import { connect }      from 'react-redux'
import { useSwipeable } from 'react-swipeable'

import style from './style.module.scss'


const mapStateToProps = null

export default connect( mapStateToProps )(( props ) => {

    const slides     = props.items
    const { length } = slides
    const lastStep   = length - 1
    const firsStep   = 0

    const [ step, set_step ] = useState(firsStep)
    
    return(
        <section className={ `${style.slider} ${props.color ? style[props.color] : ""}` }>
            <div className={ style.slides }>

                {slides.map(( Slide, key ) => {
                    
                    const swipeHandlers = useSwipeable({
                        onSwipedLeft:  (e) => ( step === lastStep ) ? set_step( firsStep ) : set_step( step + 1 ),
                        onSwipedRight: (e) => ( step === firsStep ) ? set_step( lastStep ) : set_step( step - 1 )
                    })

                    return (
                        <article { ...swipeHandlers } key={ key } style={{ transform: `translateX(${( step * -100)}%)` }} className={ style.slide_wrapper }>
                            <Slide className={style.slide}/>
                        </article>
                    )
                })}
            </div>

            <div className={ style.slidesControls }>

                {slides.map(( slide, key ) => {
                    return (
                        <button 
                            onClick={() => set_step( key )} 
                            key={ key } 
                            className={`${ style.slidesControls_button } ${( key === step ) ? style.current : "" }`}
                        />
                    )
                })}
            </div>
        </section>
    )
})