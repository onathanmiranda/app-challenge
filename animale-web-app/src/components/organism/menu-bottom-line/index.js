import { IconChat, IconLocation }   from '../../atom/icon'
import LinkHorizontal               from '../../molecule/link-horizontal'

import style from './style.module.scss'

export default function MenuBottomLine(props){
    return(
        <section className={style.menu}>

            <LinkHorizontal to="" icon={ IconLocation }>
                Encontre uma loja
            </LinkHorizontal>
            
            <LinkHorizontal to="" icon={ IconChat }>
                 Fale com um vendedor
            </LinkHorizontal>
        </section>
    )
}