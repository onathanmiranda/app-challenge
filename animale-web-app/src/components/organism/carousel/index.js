import { Link } from 'react-router-dom'

import Title from '../../atom/title'

import style from './style.module.scss'

export default function Carousel( props ){
    
    const { items, title, collectionLink } = props
    
    return (
        <section className={`${ style.section } ${ props.className || "" }`}>
            
            {(Boolean(title) || Boolean(collectionLink)) && 
                <header className={style.header}>
                    {Boolean( title ) && 
                        <Title>{ title }</Title>
                    }
                    {Boolean( collectionLink ) && 
                        <Link className={style.collectionLink} to={ collectionLink }>Ver tudo</Link>
                    }
                </header>
            }
            
            <section className={ style.items }>
                {items.map(( Item, key ) => { 
                    return(
                        <div key={ key } className={ style.item_wrapper }>
                            <Item className={ style.item }/>
                        </div>
                    )
                })}
            </section>
        </section>
    )
}