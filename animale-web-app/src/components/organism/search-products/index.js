import React, { useEffect, useState } from 'react'
import { connect }          from 'react-redux'
import { withRouter }       from 'react-router-dom'

import { IconSearch }   from '../../atom/icon'
import InputText        from '../../atom/input-text'
import ProductCard      from '../../molecule/card-product'

import { setQuery, searchProducts, clearSearch } from '../../../store/search'

import style from './style.module.scss'
import updateQueryParam from '../../../utils/updateQueryParam'


const mapStateToProps = ( state ) => ({
    search: state.search,
    products: state.products
})

const mapDispatchToProps = ( dispatchEvent ) => ({
    setQuery: ( args )          => dispatchEvent( setQuery( args )),
    searchProducts: ( args )    => dispatchEvent( searchProducts( args )),
    clearSearch: ( args )       => dispatchEvent( clearSearch( args ))
})

export default withRouter(connect( mapStateToProps, mapDispatchToProps )(( props ) => {

    const [ lastQuery, setLastQuery ] = useState(props.search.query)

    useEffect(() => {
        if(props.search.query !== "") props.searchProducts()
    // eslint-disable-next-line
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault()
        props.searchProducts()
        props.history.push(updateQueryParam(props.search.URLSearchParam, props.search.query))
        setLastQuery(props.search.query)
    }

    const handleOnChange = (value) => {
        if(value === "") {
            clearSearch()
        }
        props.setQuery(value)
    }

    const clearSearch = () => {
        props.clearSearch()
        props.history.push(updateQueryParam(props.search.URLSearchParam, ""))
    }
    
    
    const { query, result } = props.search

    const hasQuery      = query !== ""
    const hasResults    = result.length > 0
    const hasNoResults  = result.length === 0

    return (
        <section>
            <form onSubmit={ handleSubmit } className={ style.form }>

                <fieldset className={style.fieldset}>

                    <IconSearch className={style.IconSearch}/>
                    <InputText className={style.input} value={ props.search.query } onChange={ handleOnChange } placeholder="Busque por produtos..." />
                    {props.search.query && 
                        <button className={style.clear} type="button" onClick={clearSearch}>X</button> 
                    }
                </fieldset>
            </form>
            {hasQuery && 
                <>
                    {hasResults && 

                        <section className={ style.results }>
                            {props.search.result.map(( product, key ) => (
                                <ProductCard key={key} product={product} />
                            ))}
                        </section>
                    }
                    {hasNoResults && 

                        <section className={ style.notFound }>
                            Nenhum produto encontrado com "{lastQuery}"
                        </section>
                    }
                </>
            }
        </section>
    )
}))