import { Helmet }            from 'react-helmet'
import Header           from '../../organism/header'
import MenuBottomLine   from '../../organism/menu-bottom-line'

import Hero             from './hero'
import NewIn            from './new-in'
import NewCollection    from './new-collection'

import style from './style.module.scss'

export default function Home(){
    return (
        <>
            <Helmet>
                <title>Animale</title>
                <meta name="description" content="Explore o futuro com a nova coleção da ANIMALE. Crie produções autênticas com blusas, calças, vestidos e muito mais, e se prepare para o amanhã! Confira!" />
            </Helmet>
            <main className={ style.main }>
                <Header color="white" className={ style.header }/>
                <Hero />
                <NewIn />
                <NewCollection />
                <MenuBottomLine />
            </main>
        </>
    )
}