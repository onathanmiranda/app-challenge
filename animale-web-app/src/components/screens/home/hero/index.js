import LinkCTA  from '../../../atom/link-cta'
import Slider   from '../../../organism/slider'

import newWaveBackground from './editorial_pic.png'

import style from './style.module.scss'

const Slide1 = (props) => (
    <div className={`${style.slide} ${props.className || ""}`} style={{ backgroundImage: `url(${newWaveBackground})`}}>
        <p className={ style.slide_subtitle }>Preview</p>
        <h2 className={ style.slide_title }>New Wave</h2>
        <LinkCTA className={ style.slide_callToAction } to="/categorias/new-wave">Shop Now</LinkCTA>
    </div>
)

export default function HomeHero(){
    return (
        <Slider items={[ Slide1, Slide1, Slide1 ]} />
    )
}