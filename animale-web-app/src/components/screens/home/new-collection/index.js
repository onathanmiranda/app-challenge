import CategoryCard from '../../../molecule/card-category'

import style from './style.module.scss'

import newCollectionBG from './new-collection.png'

export default function NewCollection(){
    return (
        <section className={ style.section }>
            <CategoryCard className={ style.banner } to="/categorias/colecao" title="Nova Coleção" subtitle="Preview" featuredImage={newCollectionBG} />
        </section>
    )
}