import Carousel     from '../../../organism/carousel'
import CardCategory from '../../../molecule/card-category'

import HotTrendsBG  from './hot-trends.png'
import Jaquetas     from './jaquetas.png'

import style from './style.module.scss'

const categories = [
    {
        title: "Hot Trends",
        featuredImage: HotTrendsBG,
        link: "categorias/hot-trends"
    },
    {
        title: "Jaquetas",
        featuredImage: Jaquetas,
        link: "categorias/jaquetas"
    }
]

const items = categories.map(category => {
    return () => {
        return <CardCategory to={ category.link } title={ category.title } featuredImage={ category.featuredImage }/>
    }
})

export default function NewIn(){
    return(
        <Carousel className={ style.carousel } title="New In" collectionLink="#" items={ items }/>
    )
}