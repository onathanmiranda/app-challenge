import blusas   from './blusas.png'
import jaquetas from './jaquetas.png'
import tricot   from './tricot.png'
import malha    from './malha.png'
import shop     from './shop.png'

import Carousel             from '../../../organism/carousel'
import RoundCategoryLink    from '../../../molecule/link-category-round'

import style from './style.module.scss'

//Categories should ideally come from API
let categoriesToBeDisplayed = [
    () => <RoundCategoryLink title="Malha" img={malha} to="/categorias/colecao/malha" />,
    () => <RoundCategoryLink title="Jaquetas" img={jaquetas} to="/categorias/colecao/jaqueta" />,
    () => <RoundCategoryLink title="Tricot" img={tricot} to="/categorias/colecao/tricot" />,
    () => <RoundCategoryLink title="Blusas" img={blusas} to="/categorias/colecao/top/blusa" />,
    () => <RoundCategoryLink title="Shop" img={shop} to="/categorias/shop" />
]

export default function TopCategories(props){
    return (
        <Carousel className={style.carousel} items={ categoriesToBeDisplayed } />
    )
}