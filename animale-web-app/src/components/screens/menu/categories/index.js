import acessorios   from './acessorios.png'
import colecao      from './colecao.png'
import inside       from './inside.png'
import intimates    from './intimates.png'
import novidades    from './novidades.png'
import oro          from './oro.png'
import sale         from './sale.png'

import HorizontalCategoryLink    from '../../../molecule/link-category-horizontal'

import style from './style.module.scss'

//Categories should ideally come from API
let categoriesToBeDisplayed = [
    { 
        title: "Coleção",
        img: colecao,
        to: "/categorias/colecao" 
    },
    { 
        title: "Novidades",
        img: novidades,
        to: "/categorias/novidades" 
    },
    { 
        title: "Acessórios",
        img: acessorios,
        to: "/categorias/acessorios"
    },
    { 
        title: "Intimates",
        img: intimates,
        to: "/categorias/intimates"
    },
    { 
        title: "Sale",
        img: sale,
        to: "/categorias/sale"
    },
    { 
        title: "Animale Oro",
        img: oro,
        to: "/categorias/animale-oro",
        className: style.animaleOro
    },
    { 
        title: "",
        img: inside,
        to: "/categorias/inside-animale",
        className: style.insideAnimale
    },
]

export default function TopCategories(props){
    return (
        <section className={ style.section }>
            { categoriesToBeDisplayed.map(( category, index ) => <HorizontalCategoryLink key={index} { ...category } />)}
        </section>
    )
}