import { Helmet }   from 'react-helmet'
import { connect }  from 'react-redux'

import Header           from '../../organism/header'
import ProductsSearch   from '../../organism/search-products'

import TopCategories from './categories-top'
import Categories    from './categories'

import style from './style.module.scss'


const mapStateToProps = (state) => ({ search: state.search })

export default connect(mapStateToProps)(function Menu(props){

    const showCategories = props.search.result === false

    return (
        <>
            <Helmet>
                <title>Produtos | Animale</title>
                <meta name="description" content="Explore o futuro com a nova coleção da ANIMALE. Crie produções autênticas com blusas, calças, vestidos e muito mais, e se prepare para o amanhã! Confira!" />
            </Helmet>
            <main className={style.main}>
                <Header title="Menu" />
                    <ProductsSearch />
                    {showCategories && 
                        <>
                            <TopCategories/>
                            <Categories />
                        </>
                    }
            </main>
        </>
    )
})