import { useEffect }    from 'react'
import { Helmet }       from 'react-helmet'
import { withRouter }   from 'react-router-dom'
import { connect }      from 'react-redux'

import Slider   from '../../organism/slider'
import Header   from '../../organism/header'

import formatCurrency from '../../../utils/formatCurrency'

import { getByLinkText, resetState } from '../../../store/products'

import style from './style.module.scss'

const mapStateToProps = (state, props) => {

    const [ product ]   = state.products.items
    const { requesting } = state.products.requesting
    
    return { 
        product,
        requesting
    }
}

const mapDispatchToProps = (dispatchEvent) => ({
    getByLinkText: (args) => dispatchEvent(getByLinkText(args)),
    resetState: (args) => dispatchEvent(resetState(args))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(
    
    function Product(props){
    
        const product   = Boolean(props.product) ? props.product : false
        
        const isLoading = !product && props.requesting
        const notFound  = !product && !props.requesting
        
        const featuredImages = product ? product.items[0].images.map(( image ) => () => {
            return <img className={ style.featuredImage } src={ image.imageUrl } alt={ image.imageText } />
        }) : false
       
        const { commertialOffer }   = product ? product.items[0].sellers[0] : { commertialOffer: false }
        const price                 = product ? commertialOffer.Price : false
        //const priceWithoutDiscount  = product ? commertialOffer.PriceWithoutDiscount : false
        //const showfullPrice         = product ? price !== priceWithoutDiscount : false
        const isAvailable           = product ? Boolean( commertialOffer.AvailableQuantity ) : false
        
        const maxInstallmentsNumber         = product ? commertialOffer.Installments.reduce(( acc, installment ) => Math.max( acc, installment.NumberOfInstallments ), 1) : false
        const [ installment ]               = product ? commertialOffer.Installments.filter((installment) => installment.NumberOfInstallments === maxInstallmentsNumber ) : [ false ]
        const NumberOfInstallments          = installment ? installment.NumberOfInstallments : 0
        const TotalValuePlusInterestRate    = installment ? installment.TotalValuePlusInterestRate : 0
        const installmentValue              = installment ? (TotalValuePlusInterestRate / NumberOfInstallments).toFixed(2) : 0
        
        useEffect(() => {
            props.getByLinkText({ linkText : props.match.params.linkText })

            return () => props.resetState()
        // eslint-disable-next-line
        }, [])
        return (
            <>
                <Helmet>
                    <title>{ product ? `${product.productName} | Animale` : "Produto não encontrado | Animale" }</title>
                    <meta name="description" content={ product ? product.metaTagDescription : "" } />
                </Helmet>

                <main className={style.main}>

                    <Header className={ style.header } back={ true } />

                    {product &&
                        <>
                            <Slider color="black" className={ style.slider } items={ featuredImages } />
                            <div className={style.productInfos}>
                                <h1 className={ style.title }>{ product.productName }</h1>

                                {isAvailable && 
                                    <>
                                        <p className={ style.price }>{ formatCurrency( price ) }</p>
                                        {Boolean(installment) && 
                                            <p className={ style.installments }>ou {NumberOfInstallments}x de {formatCurrency( installmentValue )} </p> }
                                    </>
                                }
                                {!isAvailable &&
                                    <p className={ style.unavailable }>
                                        Esgotado
                                    </p>
                                }
                            </div>
                        </>
                    }

                    {isLoading &&
                        <div className={ style["h-padding"] }>
                            Carregando...
                        </div>
                    }

                    {notFound &&
                        <div className={ style["h-padding"] }>
                            Produto não encontrado
                        </div>
                    }
                </main>
            </>
        )
    }
))