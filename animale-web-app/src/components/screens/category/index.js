import { useEffect }    from 'react'
import { Helmet }       from 'react-helmet'
import { withRouter }   from 'react-router-dom'
import { connect }      from 'react-redux'
import slugify          from 'slugify'

import ProductCard  from '../../molecule/card-product'
import Header       from '../../organism/header'

import { getByCategory, resetState } from '../../../store/products'

import style from './style.module.scss'


const slugfy = (str) => slugify( str, { lower: true, strict: true })

const mapStateToProps = (state, props) => {
    
    const { pathname }                              = props.location
    const { items, page, totalPages, requesting }   = state.products
    const [ firstItem ]                             = items

    const category          = pathname.replace("/categorias", "") + "/"
    
    const hasProducts       = Boolean(items.length)

    const categoryName      = hasProducts ? 
                                firstItem.categories.filter(( cat ) => slugfy( cat ) === slugfy( category ))[0].split("/").filter(( cat ) => cat !== "").pop() :
                                pathname.split("/").filter(( cat ) => cat !== "").pop().replace("-", " ")

    return ({
        products: items,
        category: {
            categoryName,
            categoryPath: category
        },
        requesting,
        hasProducts,
        page,
        totalPages
    })
}

const mapDispatchToProps = (dispatchEvent) => {
    return {
        getByCategory: (args) => dispatchEvent(getByCategory(args)),
        resetState:    (args) => dispatchEvent(resetState(args))
    }
}

export default withRouter(connect( mapStateToProps, mapDispatchToProps )(
    
    function Category(props){
        
        const { products, category, hasProducts, requesting, totalPages, page } = props
        const { categoryName, categoryPath } = category
        
        const isLoading     = !hasProducts && requesting
        const noProducts    = !hasProducts && !requesting
        const hasLoadedAll  = page > totalPages
        
        useEffect(() => {
            props.getByCategory({ category: categoryPath })

            return () => props.resetState()
        // eslint-disable-next-line
        }, [])
        
        const loadMore = (e) => {
            props.getByCategory({ category: categoryPath })
        }
        
        return (
            <>
                <Helmet>
                    <title>{ categoryName } | Animale</title>
                    <meta name="description" content="Explore o futuro com a nova coleção da ANIMALE. Crie produções autênticas com blusas, calças, vestidos e muito mais, e se prepare para o amanhã! Confira!" />
                </Helmet>
                <main className={style.main}>
                    <div className={style.transitionGroup}>
                        <Header back={ true } title={ categoryName } />

                        {hasProducts && 
                            <>
                                <section className={ style.products }>
                                    {products.map(( product, index ) => <ProductCard key={ index } product={ product } /> )}
                                </section>
                                
                                {!hasLoadedAll && 
                                    <button className={ style.carregarMais } onClick={loadMore}>Ver mais</button>}
                            </>
                        }
                        {isLoading &&
                            <section className={ style.notFound }>
                                {`Carregando...`}
                            </section>
                        }
                        {noProducts &&
                            <section className={ style.notFound }>
                                {`Nenhum produto encontrado na categoria "${ categoryName }".`}
                            </section>
                        }
                    </div>
                </main>
            </>
        )
    }
))