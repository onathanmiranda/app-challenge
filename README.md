# Desafio Desenvolvedor App

## Rodando o projeto
* `cd` na pasta `animale-web-app`
* Instale todas as dependências usando yarn ou npm
* Em seguida use o comando `yarn start`
* Abra [http://localhost:3000](http://localhost:3000) no navegador para visualizar o projeto.

## Instruções
* Dê um fork neste projeto;
* Desenvolva as telas do app (layout disponível em: https://tinyurl.com/ya4txclk)
* Atualize o readme com as instruções necessárias para rodar o seu código;
* Faça um pull request.


##### Sugestões de implementação
* Interação com JSON para renderizar os produtos (você vai encontrar um mockup em src/data/products.json)
* Filtro de produtos funcional
* Botão de carregar mais produtos na página de catálogo (tela 3)
* Micro animações e micro interações

##### Dicas
* Evite usar linguagens, ferramentas e metodologias que não domine;
* Não esqueça de manter o package atualizado com os módulos necessários para rodar seu projeto;
* Se for implementar algum componente que não está nas telas, tente manter a consistencia do layout


###### Dúvidas: vinicius.diniz@somagrupo.com.br
